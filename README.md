* To run the tests in this project:

    * Create a suitable user with readWrite privs on a database of your choice in your local mongo server

    * Copy config/default.js to config/local.js and edit the value of the db.url field therein to match the user, password and database that you created

* Run once:
	
  > $ npm install
  
* Run the tests:

  > $ npm test

* Alternatively, you can exercise the ingester by running:

  > $ node index

* TODOS:

    * I have used node-config to do manual Dependency Injection in a separate configuration file, and use the node-config overrides for the various DEV/QA\*/PROD environments. I ran out of time for this.
	
	* If a messaging queue is avail, I would probably not include the email scheduling functionality in the PatientFileIngester, but would instead emmit a 'PatientImportEvent' to the local messaging bus with the data, and let an event handler deal with the scheduling. This would ensure that the PatientFileIngester is focused on the members.csv file and PatientService, and not have to be changed when marketing decides to do a different notification strategy.

