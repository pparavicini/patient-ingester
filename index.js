'use strict';

const 
	config = require('config'),
	mongoose = require('mongoose'),
	EmailSchema = require('./src/email-schema').EmailSchema, 
	EmailService = require('./src/email-service'),
	Email = mongoose.model('Email', EmailSchema),
	PatientFileIngester = require('./src/patient-file-ingester'),
  PatientSchema = require('./src/patient-schema').PatientSchema,
  Patient = mongoose.model('Patient', PatientSchema),
  PatientService = require('./src/patient-service');

const patientService= new PatientService(Patient);
const emailService = new EmailService(Email);
const ingester = new PatientFileIngester (patientService, emailService);
const FILE = './test/members.psv';
const CONN_URL = config.db.url;
	
main()
	.then(result => {
		console.log('Run result: %j', result);
		process.exit(0);
	})
	.catch( err => {
		console.log(err);
		process.exit(1);
	});

async function main() {
  await mongoose.connect(CONN_URL);
	await Patient.deleteMany();
	await Email.deleteMany();
	// console.log('File Stats', ingester.analyze(FILE));
	const stats = await ingester.analyze(FILE);
	console.log(stats);
	return ingester.ingest(FILE);
}
