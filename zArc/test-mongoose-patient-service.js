'use strict';

const 
	mongoose = require('mongoose'),
  PatientSchema = require('../src/patient-schema').PatientSchema,
  Patient = mongoose.model('Patient', PatientSchema),
  PatientService = require('../src/patient-service');

const patientService = new PatientService(Patient);

var patient = new Patient({
	memberId: mongoose.mongo.ObjectId(),
	programId: '50777445',
	cardNum: '53434323',
	firstName: 'LOAD',
	lastName: 'TEST 17',
	dateOfBirth: '04/29/1981',
	address: {
		line1: '3100 S Ashley Drive',
		line2: '',
		city: 'Chandler',
		state: 'AZ',
		zipCode: '85286'
	},
	email: 'test17@humancaresystems.com',
	phone: {
		mobile: '1234567890',
		home: ''
	},
	consent: false,
	dataSource: 'WEB 3RD PARTY'
})


main().catch( err => {
	console.log(err);
	process.exit(1);
});

async function main() {
	console.log('patient instanceof Patient', patient instanceof Patient);
	console.log('patient.memberId', patient.memberId);
	// console.log('patient: %s', patient);
  await mongoose.connect('mongodb://patient-ingester:somePass@localhost/human-care?authSource=admin');
	// await patient.save();
	// Patient.create(patient);
	await patientService.create(patient);

	const patients = await Patient.find();
	// console.log(patients);
	console.log('#patients:', patients.length);
	process.exit(0);
}
