'use strict';

const 
	_ = require('lodash'),
	config = require('config'),
	mongoose = require('mongoose'),
	{DateTime} = require('luxon'),
	EmailSchema = require('../src/email-schema').EmailSchema, 
	EmailService = require('../src/email-service'),
	Email = mongoose.model('Email', EmailSchema),
	PatientFileIngester = require('../src/patient-file-ingester'),
  PatientSchema = require('../src/patient-schema').PatientSchema,
  Patient = mongoose.model('Patient', PatientSchema),
  PatientService = require('../src/patient-service');

const emailService = new EmailService(Email);
const patientService = new PatientService(Patient);
const ingester = new PatientFileIngester(patientService, emailService);

const CONN_URL = config.db.url;

const FILE = './test/members.psv';
const NUM_PATIENTS = 18;
const NUM_MISSING_FIRSTNAME = 2;
const NUM_CONSENT  =  8;
const NUM_CONSENT_WITH_EMAIL = 7;
const NUM_EMAILS_PER_PATIENT =4;
const NUM_EMAILS   = 28;

// let PATIENT = new Patient({
let PATIENT = {
	_id: mongoose.mongo.ObjectId(),
	memberId: 12345,
	programId: 50777445,
	cardNum: 53434323,
	firstName: 'LOAD',
	lastName: 'TEST 17',
	dateOfBirth: '04/29/19871',
	address: {
		line1: '3100 S Ashley Drive',
		line2: '',
		city: 'Chandler',
		state: 'AZ',
		zipCode: '85286'
	},
	email: 'test17@humancaresystems.com',
	phone: {
		mobile: '1234567890',
		home: ''
	},
	consent: false,
	dataSource: 'WEB 3RD PARTY',
};

beforeAll(async () => {
  await mongoose.connect(CONN_URL);
});

afterAll(async () => {
  await mongoose.disconnect();
});

describe('Test CRUD methods', () => {

	beforeAll(async () => {
		await Patient.deleteMany();

		// console.log('PATIENT before', PATIENT);
		await patientService.create(PATIENT);
	});

	test('load a patient by memberId', async () => {
		const aPatient = await patientService.findByMemberId(PATIENT.memberId);
		// console.log('patient persisted:', aPatient);
		expect(aPatient.memberId).toEqual(PATIENT.memberId);
		expect(aPatient.firstName).toEqual(PATIENT.firstName);
		expect(aPatient.lastName).toEqual(PATIENT.lastName);
		expect(aPatient.email).toEqual(PATIENT.email);

		// mongoose adds the property __v to the POJO, 
		// which would cause the match below to fail
		delete aPatient['__v'];
		expect(aPatient).toEqual(PATIENT);
	});
});


describe('Ingest users from flat file', () => {

	beforeAll(async () => {
		await Patient.deleteMany();
		await Email.deleteMany();
		await ingester.analyze(FILE);
		console.log(ingester.stats);
		await ingester.ingest(FILE);
	});

	test('load a patient by memberId', async () => {
		const aPatient = await patientService.findByMemberId(PATIENT.memberId);
		// console.log('patient persisted:', aPatient);
		expect(aPatient.memberId).toEqual(PATIENT.memberId);
		expect(aPatient.firstName).toEqual(PATIENT.firstName);
		expect(aPatient.lastName).toEqual(PATIENT.lastName);
		expect(aPatient.email).toEqual(PATIENT.email);
	});

	test('verify patients ingested', async () => {
		let patients = await Patient.find({});
		expect(patients.length).toEqual(NUM_PATIENTS);

		patients = await Patient.find({firstName: ''});
		expect(patients.length).toEqual(NUM_MISSING_FIRSTNAME);

		patients = await Patient.find({consent: true});
		expect(patients.length).toEqual(NUM_CONSENT);

		patients = await patientService.findNotifiablePatients();
		expect(patients.length).toEqual(NUM_CONSENT_WITH_EMAIL);
	});

	test('verify emails scheduled', async () => {
		const emails = await Email.find({});
		expect(emails.length).toEqual(NUM_EMAILS);

		const patients = await patientService.findNotifiablePatients();

		for (let patient of patients) {
			let emails = await emailService.findByMemberId(patient.memberId);
			emails = _.orderBy(emails,'name');
			// console.log(emails);
			expect(emails.length).toEqual(NUM_EMAILS_PER_PATIENT);
			expect(emails[0].scheduled_date).toBeInstanceOf(Date);
			let dayOne = DateTime.fromJSDate(emails[0].scheduled_date);

			for (let i=1; i <= emails.length; i++) {
				let email = emails[i-1];
				expect(_.isString(email.id)).toBe(true);
				expect(email.name).toBe('Day ' + i);
				expect(email.scheduled_date).toBeInstanceOf(Date);
				let dateDiff = DateTime.fromJSDate(email.scheduled_date).diff(dayOne,'days').toObject().days;
				expect(dateDiff).toBe(i-1);
			}
		}
	});

});

