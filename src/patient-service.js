const _ = require('lodash');

var Factory = (function() {

	/**
	* @class PatientService
	* Service facade for calls that may be exposed via a web service API
	* @constructor
	* @param {Object} options some text
	*/
	function PatientService(aPatientModel, opts) {
		this.Patient = aPatientModel;
	}

	var proto = PatientService.prototype;

	proto.findByMemberId = async function findByMemberId(memberId) {
		if (!_.isNumber(memberId)) throw 'findByMemberId param is not a number';
		return this.Patient.findOne({memberId: memberId}).lean();
	}

	/** returns patients who opted-in and have an email address */
	proto.findNotifiablePatients = async function findNotifiablePatients() {
		return this.Patient.find({consent: true, email: {$ne:''}});
	}

	proto.insertMany = async function insertMany(patients) {
		if (!_.isArray(patients)) throw 'insertMany param is not an array';
		return this.Patient.insertMany(patients);
	}

	proto.create = async function create(patient) {
		return this.Patient.create(patient);
	}

	return PatientService;

}());

module.exports = Factory;
