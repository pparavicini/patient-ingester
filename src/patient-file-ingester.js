const 
    _ = require('lodash'),
		csv = require('fast-csv'),
		fs = require('fs'),
		{ DateTime } = require('luxon'),
		path = require('path'),
		{ v4: uuidv4 } = require('uuid')
;

const PARSE_OPTS = { 
	headers: true, 
	delimiter: '|'
};


var Factory = (function() {

	function PatientFileIngester(patientService, emailService, opts) {
		this.patientService = patientService;
		this.emailService = emailService;
		this.patients;
		this.stats;

		/* stats is instantiated lazily when the analyze method is used *
		this.stats = {
			file: aFileName,
			numRecordsInFile: 0,
			consenting: [],
			missingFirstName: [],
			missingEmailWithConsent: []
		};
		*/

		that = this;
	}

	var that;
	var proto = PatientFileIngester.prototype;

	/**
	* @method ingest
	* @return {Object}
	*/
	proto.ingest = async function ingest(aFileName) {
		console.log('Ingesting:', aFileName);

		this.patients = [];
		
		return new Promise((resolve, reject) => {
			fs.createReadStream(path.resolve(aFileName))
				.pipe(csv.parse(PARSE_OPTS))
				.transform(_transformRow)
				.on('error', error => reject(error))
				.on('data', row => this.patients.push(row))
				.on('end', rowCount => resolve(_onEnd(rowCount)));
		})
	}

	/** Read-only method that analyzes the file to be ingested */
	proto.analyze = function analyze(aFileName) {
		this.patients = [];
		this.stats = {
			file: aFileName,
			numRecordsInFile: 0,
			consenting: [],
			missingFirstName: [],
			missingEmailWithConsent: []
		};

		return new Promise((resolve, reject) => {
			fs.createReadStream(path.resolve(aFileName))
				.pipe(csv.parse(PARSE_OPTS))
				.transform(_transformRow)
				.on('error', error => reject(error))
				.on('data', row => this.patients.push(row))
				.on('end', rowCount => resolve(createStats(rowCount)));
		})

		function createStats(rowCount) {
			that.stats.numRecordsInFile = rowCount;
			return that.patients.reduce((stats, patient) => {
				if (patient.consent) stats.consenting.push(patient.memberId);
				if (!patient.firstName) stats.missingFirstName.push(patient.memberId);
				if (patient.consent && !patient.email) stats.missingEmailWithConsent.push(patient.memberId);
				return stats;
			}, that.stats);
		}
	}

	async function _onEnd(rowCount) {
		// console.log('onEnd: processed %s rows', rowCount);
		// console.log(this.patients);
		let now = DateTime.now();

		
		 // create email notification records for consenting patients with email
		 // TODO: should be represented by a public utility method 
		const emails = that.patients
			.filter(patient => patient.email && patient.consent)
			.reduce((emails, patient) => {
				let i=1;
				while (i < 5) {
					emails.push({
						id: uuidv4(),
						memberId: patient.memberId,
						name: 'Day '+i,
						scheduled_date: now.plus({days: i}).toISO()
					})
					i++;
				}
				return emails;
			}, [])

		// console.log('#emails', emails.length);
		// console.log('emails', emails);

		return Promise.all([
		  that.patientService.insertMany(that.patients),
			that.emailService.insertMany(emails)
		]);
	}

	function _transformRow(aRow) {
		// console.log(aRow);
		return {
			memberId: aRow['Member ID'],
			programId:aRow['Program Identifier'],
			cardNum:  aRow['Card Number'],
			firstName: aRow['First Name'],
			lastName: aRow['Last Name'],
			dateOfBirth:aRow['Date of Birth'], 
			address: {
				line1:  aRow['Address 1'],
				line2:  aRow['Address 2'],
				city: 	aRow['City'],
				state:  aRow['State'],
				zipCode:aRow['Zip code']
			},
			email:    aRow['Email Address'],
			phone: {
				mobile:aRow['Mobile Phone'],
				home:  aRow['Telephone number']
		},
			consent:  aRow['CONSENT'] === 'Y',
			dataSource:aRow['Data Source']
		}
	}

	return PatientFileIngester;

}())
module.exports = Factory;

