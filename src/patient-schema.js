'use strict';

const mongoose = require('mongoose');

exports.PatientSchema = new mongoose.Schema({
	memberId: {
		type: Number,
		required: true,
		index: true,
		unique: true
	},
	programId: Number,
	cardNum: Number,
	firstName: String,
	lastName: {type: String, index: true},
	address: {
		line1: String,
		line2: String,
		city: String,
		state: String,
		zipCode: String,
	},
	email: {type: String, index: true},
	phone: { 
		mobile: {type: String, index: true},
		home: String,
	},
	dateOfBirth: String,
	dataSource: String,
	consent: Boolean,
})
