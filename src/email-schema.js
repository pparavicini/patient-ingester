'use strict';

const mongoose = require('mongoose');

exports.EmailSchema = new mongoose.Schema({
	id: {
		type: String,
		required: true,
		index: true,
		unique: true
	},
	memberId: Number,
	name: String,
	scheduled_date: Date
})
