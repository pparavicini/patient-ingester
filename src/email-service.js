const _ = require('lodash') ;

var Factory = (function() {

	/**
	* @class EmailService
	* @constructor
	* @param {Object} options some text
	*/

	function EmailService(aEmailModel, opts) {
		this.Email = aEmailModel;
	}

	var proto = EmailService.prototype;

	proto.findById = async function findById(id) {
		if (!_.isString(id)) throw 'findById param is not a string';
		return this.Email.findOne({id: id}).lean();
	}

	proto.findByMemberId = async function findByMemberId(id) {
		if (!_.isNumber(id)) throw 'findByMemberId param is not a number';
		return this.Email.find({memberId: id}).lean();
	}

	proto.insertMany = async function insertMany(emails) {
		if (!_.isArray(emails)) throw 'insertMany param is not an array';
		return this.Email.insertMany(emails);
	}

	proto.create = async function create(email) {
		return this.Email.create(email);
	}

	return EmailService;

}());

module.exports = Factory;
